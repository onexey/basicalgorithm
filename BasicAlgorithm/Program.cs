﻿using System;
using System.Linq;

namespace BasicAlgorithm
{
    class Program
    {
        static bool CokOnemliIsler() { return true; }

        private static void DisardanVerilenSayininRakamlariToplami()
        {
            int DisardanSayi;
            int toplam = 0;
            Console.WriteLine("Lütfen değer giriniz: ");

            var input = Console.ReadLine();


            if (int.TryParse(input, out DisardanSayi))
            {
                /*
                    modlamak
                    String --> character --> int

             */

                string DisardanString = DisardanSayi.ToString();
                //[1,1,1]
                var disaridanArray = DisardanString.ToCharArray();

                for (int i = 0; i < disaridanArray.Length; i = i + 1)
                {
                    var charToString = disaridanArray[i].ToString();
                    var chatToStringToInt32 = Convert.ToInt32(charToString);
                    toplam = toplam + chatToStringToInt32;
                }

                foreach (var item in DisardanString.ToCharArray())
                    toplam += Convert.ToInt32(item.ToString());

                Console.WriteLine($@"Toplam: {0}", toplam);
                Console.WriteLine($"Toplam: {toplam}");
                Console.WriteLine($"Toplam: {toplam}");
                Console.WriteLine();
            }
            else
            {
                Console.WriteLine("recursive call!!!");
                DisardanVerilenSayininRakamlariToplami();
            }
        }

        static void GirilenIkiSayiyiTopla()
        {
            Console.WriteLine("İlk sayıyı girip enter a basın.");

            var ilkSayi = Console.ReadLine();
            Console.WriteLine("İkinci sayıyı girip enter a basın.");
            var ikinciSayi = Console.ReadLine();

            if (int.TryParse(ilkSayi, out var ilkSayiInt) && int.TryParse(ikinciSayi, out var ikinciSayiInt))
            {
                var result = Topla(ilkSayiInt, ikinciSayiInt);
                Console.WriteLine($"İki sayının toplamı: {result}");
            }
            else
            {
                Console.WriteLine("Maalesef");
                GirilenIkiSayiyiTopla();
            }
        }

        static void GirilenSayiyaKadarToplam()
        {
            /*
           1) N 1,N --> toplam
           */

            Console.Write("Bura başlangıç. Alt satıra geçmedim:");
            Console.WriteLine("Lütfen bir sayı giriniz: ");
            var toplam = 0;
            int disardanSayi = 0;

            var input = string.Empty;

            bool dogru = true;

            do
            {
                Console.WriteLine("Değeri şimdi giriniz:");
                input = Console.ReadLine();
                if (input == "YETER")
                {
                    dogru = false;
                    break;
                }
            } while (!int.TryParse(input, out disardanSayi));


            //if (!int.TryParse(Console.ReadLine(), out disardanSayi))
            //{
            //    Console.WriteLine("Sayı dedik!!!");
            //}

            int integerDegisken = 1;
            double doubleDegisken = 1.19849283092838029;
            decimal decimalDegisken = 1.1M;
            string stringDegisken = "Mesut & Gürcan";
            bool boolDegisken = true;
            DateTime datetimeDegisken = DateTime.Parse("2019-11-30 09:32");

            if (dogru)
            {
                for (int i = 0; i <= disardanSayi; i++)
                {
                    toplam = toplam + i;
                }

                Console.WriteLine(toplam);
            }
            else
                Console.WriteLine("Kısmet Değilmiş.");
        }

        static void Main(string[] args)
        {
            //GirilenSayiyaKadarToplam();
            // YariCapiVerilenDaireninCevresi();
            // DisardanVerilenSayininRakamlariToplami();

            //GirilenIkiSayiyiTopla();
            //GirilenIkiSayiyiCikartma();
            //GirilenIkiSayiyiCarpma();
            //GirilenIkiSayiyiBolme();
            FaktoriyelAlma();

            System.Console.ReadLine(); /* Enter karakteri oku */
            //Console.ReadKey(); --> tek karakter oku
        }

        static void FaktoriyelAlma()
        {

            Console.WriteLine("Faktöriyel için sayı girip enter a basın.");
            var ilkSayi = Console.ReadLine();

            if (int.TryParse(ilkSayi, out var ilkSayiInt))
            {
                var result = Faktoriyel(ilkSayiInt);
                Console.WriteLine($"Sayının faktöriyeli: {result}");
            }
            else
            {
                Console.WriteLine("Maalesef");
                FaktoriyelAlma();
            }
        }

        static double Faktoriyel(int sayi)
        {
            double result = 1;

            var sw = new System.Diagnostics.Stopwatch();
            sw.Start();

            //var startTime = DateTime.Now;

            for (int i = 1; i <= sayi; i++)
            {
                result *= i;
            }

            sw.Stop();
            Console.WriteLine($"Geçen Süre: {sw.ElapsedMilliseconds}ms");

            return result;
        }

        static void GirilenIkiSayiyiCarpma()
        {
            Console.WriteLine("İlk sayıyı girip enter a basın.");
            var ilkSayi = Console.ReadLine();

            Console.WriteLine("İkinci sayıyı girip enter a basın.");
            var ikinciSayi = Console.ReadLine();

            if (int.TryParse(ilkSayi, out var ilkSayiInt) && int.TryParse(ikinciSayi, out var ikinciSayiInt))
            {
                var result = Carp(ilkSayiInt, ikinciSayiInt);
                Console.WriteLine($"İki sayının carpimi: {result}");
            }
            else
            {
                Console.WriteLine("Maalesef");
                GirilenIkiSayiyiCarpma();
            }
        }
        static int Carp(int ilkSayiInt, int ikinciSayiInt)
        {
            /* 
             * 5 * 1000000
             * manual çarma 0.015529sn */
            var startTime = DateTime.Now;

            var carpim = 0;
            carpim = ilkSayiInt * ikinciSayiInt;
            //for (int i = 0; i < ikinciSayiInt; i++)
            //{
            //    carpim += ilkSayiInt;
            //}
            Console.WriteLine($"Geçen süre: {DateTime.Now.Subtract(startTime).TotalMilliseconds}sn");
            return carpim;
        }
        static void GirilenIkiSayiyiBolme()
        {
            Console.WriteLine("İlk sayıyı girip enter a basın.");
            var ilkSayi = Console.ReadLine();

            Console.WriteLine("İkinci sayıyı girip enter a basın.");
            var ikinciSayi = Console.ReadLine();

            if (int.TryParse(ilkSayi, out var ilkSayiInt) && int.TryParse(ikinciSayi, out var ikinciSayiInt))
            {
                var result = Bol(ilkSayiInt, ikinciSayiInt);
                Console.WriteLine($"İki sayının bolumu: {result}");
            }
            else
            {
                Console.WriteLine("Maalesef");
                GirilenIkiSayiyiBolme();
            }

        }
        static decimal Bol(int ilkSayiInt, int ikinciSayiInt)
        {
            var result = (decimal)ilkSayiInt / ikinciSayiInt;
            return result;
        }
        static void GirilenIkiSayiyiCikartma()
        {
            Console.WriteLine("İlk sayıyı girip enter a basın.");
            var ilkSayi = Console.ReadLine();

            Console.WriteLine("İkinci sayıyı girip enter a basın.");
            var ikinciSayi = Console.ReadLine();

            if (int.TryParse(ilkSayi, out var ilkSayiInt) && int.TryParse(ikinciSayi, out var ikinciSayiInt))
            {
                var result = Cikart(ilkSayiInt, ikinciSayiInt);
                Console.WriteLine($"İki sayının farkı: {result}");
            }
            else
            {
                Console.WriteLine("Maalesef");
                GirilenIkiSayiyiCikartma();
            }
        }
        static int Cikart(int ilkSayiInt, int ikinciSayiInt)
        {
            return ilkSayiInt - ikinciSayiInt;
        }

        private static int Topla(int ilkSayiInt, int ikinciSayiInt)
        {
            return ilkSayiInt + ikinciSayiInt;
        }

        static void YariCapiVerilenDaireninCevresi()
        {
            Console.WriteLine("Yarı çapı giriniz : ");

            if (int.TryParse(Console.ReadLine(), out int YariCap))
            {
                /* 2 pi r*/

                var Cevre = (2 * System.Math.PI) * YariCap;

                Console.WriteLine($@"Hesaplanan çevre : { Cevre } ");
            }
            else
            {
                /* recursive*/
                Console.WriteLine("Recursive Call!!!");
                YariCapiVerilenDaireninCevresi();
            }
        }
    }
}
