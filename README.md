# Suggestions

## Instructors

Gürcan Biçer - <gurcan[@]gmail.com>

Mesut Soylu - <mesut[@]mesutsoylu.com>

## Resources

- [r/learnprogramming](https://www.reddit.com/r/learnprogramming/wiki/faq#wiki_where_do_i_find_good_learning_resources.3F)
- [C# Tutorials](https://docs.microsoft.com/en-us/dotnet/csharp/tutorials/)
- [Microsoft Learn](https://docs.microsoft.com/en-us/learn/)
- [The C# Yellow Book - Rob Miles](https://www.robmiles.com/s/CSharp-Book-2019-Refresh.pdf)
- [Code Complete - Steve McConnell](https://www.amazon.com/Code-Complete-Practical-Handbook-Construction/dp/0735619670)
- [Clean Code - Robert C. Martin](https://www.amazon.com/Clean-Code-Handbook-Software-Craftsmanship/dp/0132350882)
- [The Tao Of Programming](http://www.textfiles.com/100/taoprogram.pro)

## Practice

- [LeetCode](https://leetcode.com/)
- [Project Euler](https://projecteuler.net/archives)
- [HackerRank](https://www.hackerrank.com/)

## Video

- [Çizgi-Tagem e-Kampüs](https://www.cizgi-tagem.org/e-kampus-egitim/)
- [Channel 9](https://channel9.msdn.com/)
