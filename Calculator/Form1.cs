﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btn1_Click(object sender, EventArgs e)
        {

            Button txt = (Button)sender;

            txtSayi1.Text += txt.Text;


        }

        private void btnX_Click(object sender, EventArgs e)
        {
            txtSayi1.Text = string.Empty;
            //txtSayi1.Text = "";

        }
        int Sayi1 = default;
        int Sayi2 = default;

        private void btnTopla_Click(object sender, EventArgs e)
        {
            IlkSayiyiDegiskeneAl(1);
        }

        private void btnEsittir_Click(object sender, EventArgs e)
        {
            if (int.TryParse(txtSayi1.Text, out Sayi2))
            {
                //if (islemler == 1)
                //    txtResult.Text = (Sayi1 + Sayi2).ToString();
                //else if (islemler == 2)
                //    txtResult.Text = (Sayi1 * Sayi2).ToString();

                switch (islemler)
                {
                    case 1:
                        txtResult.Text = (Sayi1 + Sayi2).ToString();
                        txtYapilanislem.Text = "Toplama";
                        break;
                    case 2:
                        txtResult.Text = (Sayi1 * Sayi2).ToString();
                        txtYapilanislem.Text = "Çarpma";

                        break;
                    case 3:
                        txtResult.Text = ((decimal)Sayi1 / Sayi2).ToString();
                        txtYapilanislem.Text = "Bölme";

                        break;
                    case 4:
                        txtResult.Text = (Sayi1 - Sayi2).ToString();
                        txtYapilanislem.Text = "Fark";

                        break;

                    default:
                        break;

                }
                txtSayi1.Text = string.Empty;
                Sayi1 = default;
                Sayi2 = default;
            }
        }

        int islemler = 0;
        /*
         1 = toplama
         2 = çarpma
         3 = bölme
         4 = çıkartma
             
             */


        private void IlkSayiyiDegiskeneAl(int islem)
        {
            if (txtSayi1.Text == string.Empty)
            {

                MessageBox.Show("Sayı girmediniz işlem iptal edilecek.");
                return;
            }


            if (int.TryParse(txtSayi1.Text, out Sayi1))
            {
                txtSayi1.Text = string.Empty;
                islemler = islem;
            }
            else
            {
                MessageBox.Show("Lütfen bir sayı giriniz!");
            }
        }

        private void btnCarp_Click(object sender, EventArgs e)
        {
            IlkSayiyiDegiskeneAl(2);
        }

        private void btnBol_Click(object sender, EventArgs e)
        {
            IlkSayiyiDegiskeneAl(3);
        }

        private void btnCikartma_Click(object sender, EventArgs e)
        {
            IlkSayiyiDegiskeneAl(4);
        }
    }
}
