﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fihrist
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public List<Kisi> ButunKisiler = new List<Kisi>();


        private void btnPhoneAdd_Click(object sender, EventArgs e)
        {
            if (txtPhone.Text != string.Empty)
                lstPhones.Items.Add(txtPhone.Text);

            txtPhone.Text = string.Empty;

        }

        private void Temizle()
        {
            txtName.Text = string.Empty;
            txtLastName.Text = string.Empty;
            txtMail.Text = string.Empty;
            txtPhone.Text = string.Empty;
            dtpBirthDate.Value = DateTime.Now;
            lstEmails.Items.Clear();
            lstPhones.Items.Clear();

        }

        private void btnMailAdd_Click(object sender, EventArgs e)
        {
            if (txtMail.Text != string.Empty)
                lstEmails.Items.Add(txtMail.Text);

            txtMail.Text = string.Empty;
            txtMail.Focus();
        }

        private void btnPhoneRemove_Click(object sender, EventArgs e)
        {
            if (lstPhones.SelectedIndex != -1)
                lstPhones.Items.RemoveAt(lstPhones.SelectedIndex);
        }

        private void btnMailRemove_Click(object sender, EventArgs e)
        {
            if (lstEmails.SelectedIndex != -1)
                lstEmails.Items.RemoveAt(lstEmails.SelectedIndex);
        }

        private void btnKaydet_Click(object sender, EventArgs e)
        {
            if (txtName.Text == string.Empty || txtLastName.Text == string.Empty)
            {
                MessageBox.Show("isim veya soyad boş!");
                return;
            }

            var kisi = new Kisi();
            kisi.Name = txtName.Text;
            kisi.LastName = txtLastName.Text;
            kisi.BirthDate = dtpBirthDate.Value;
            kisi.ID = Guid.NewGuid();

            if (lstEmails.Items.Count > 0)
            {
                kisi.eMails = new List<Mail>();
                foreach (var item in lstEmails.Items)
                {
                    var email = new Mail();
                    email.eMail = item.ToString();
                    email.KisiID = kisi.ID;
                    kisi.eMails.Add(email);
                }
            }

            if (lstPhones.Items.Count > 0)
            {
                kisi.Phones = new List<Phone>();
                foreach (var item in lstPhones.Items)
                {
                    var p = new Phone
                    {
                        KisiID = kisi.ID,
                        Number = item.ToString()
                    };
                    kisi.Phones.Add(p);
                }
            }


            bool Bulundu = false;
            if (ButunKisiler != null && ButunKisiler.Count > 0)
            {

                foreach (var tekilkisi in ButunKisiler)
                {
                    foreach (var phone in tekilkisi.Phones)
                    {
                        foreach (var yeniPhone in kisi.Phones)
                        {
                            if (phone.Number == yeniPhone.Number)
                            {
                                Bulundu = true;
                                break;
                            }
                            //if (Bulundu) break;
                        }
                        if (Bulundu) break;
                    }
                    if (Bulundu) break;
                }
            }

            if (!Bulundu)
            {
                lstKayitlilar.Items.Add(kisi);
                ButunKisiler.Add(kisi);

                try
                {
                    string output = JsonConvert.SerializeObject(kisi);
                    System.IO.File.WriteAllText($@"C:\X\kisiler\{kisi.ID}.txt", output);

                    Temizle();
                }
                catch (Exception)
                {
                    MessageBox.Show("Dosyaya kaydederken hata oldu");
                }

            }
            else
            {
                MessageBox.Show("Bu telefon daha önceden kayıtlı!");
            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Temizle();
            OncekiKayitlariOku();


        }

        private void OncekiKayitlariOku()
        {
            if (!System.IO.Directory.Exists($@"C:\X\Kisiler\"))
            {
                System.IO.Directory.CreateDirectory($@"C:\X\Kisiler\");
            }

            foreach (var item in System.IO.Directory.GetFiles($@"C:\X\Kisiler\"))
            {
                var stringOku = System.IO.File.ReadAllText(item);
                var kisi = JsonConvert.DeserializeObject<Kisi>(stringOku);
                lstKayitlilar.Items.Add(kisi);

            }


        }

        private void lstKayitlilar_SelectedIndexChanged(object sender, EventArgs e)
        {
            Temizle();
            if (lstKayitlilar.SelectedIndex == -1)
                return;

            Kisi kisi = (Kisi)lstKayitlilar.Items[lstKayitlilar.SelectedIndex];

            txtName.Text = kisi.Name;
            txtLastName.Text = kisi.LastName;
            dtpBirthDate.Value = kisi.BirthDate;

            if (kisi.eMails != null && kisi.eMails.Count > 0)
            {
                foreach (var item in kisi.eMails)
                {
                    lstEmails.Items.Add(item.eMail);
                }
            }
            if (kisi.Phones != null && kisi.Phones.Count > 0)
            {
                foreach (var item in kisi.Phones)
                {
                    lstPhones.Items.Add(item.Number);
                }
            }


        }

        private void btnTemizle_Click(object sender, EventArgs e)
        {
            Temizle();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                var kisi = (Kisi)lstKayitlilar.SelectedItem;
                lstKayitlilar.Items.RemoveAt(lstKayitlilar.SelectedIndex);
                //var dosyalar = System.IO.Directory.GetFiles($@"C:\X\Kisiler");
                if (System.IO.File.Exists($@"C:\X\Kisiler\{kisi.ID}.txt"))
                    System.IO.File.Delete($@"C:\X\Kisiler\{kisi.ID}.txt");
            }
            catch (Exception ex)
            {
                throw;
            }

        }
    }
}
