﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fihrist
{
    public class Kisi
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }

        public string FullName
        {
            get
            {
                return Name + " " + LastName;
            }
        }

        public List<Phone> Phones { get; set; }
        public DateTime BirthDate { get; set; }
        public List<Mail> eMails { get; set; }

        public override string ToString()
        {
            return $@"Tam Adı : {FullName}";


        }


    }

    public class Phone
    {
        public Guid KisiID { get; set; }

        public string Number { get; set; }
    }

    public class Mail
    {
        public Guid KisiID { get; set; }
        public string eMail { get; set; }
    }

}
