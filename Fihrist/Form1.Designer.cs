﻿namespace Fihrist
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lstPhones = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lstEmails = new System.Windows.Forms.ListBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtLastName = new System.Windows.Forms.TextBox();
            this.dtpBirthDate = new System.Windows.Forms.DateTimePicker();
            this.btnKaydet = new System.Windows.Forms.Button();
            this.btnPhoneAdd = new System.Windows.Forms.Button();
            this.btnMailAdd = new System.Windows.Forms.Button();
            this.btnPhoneRemove = new System.Windows.Forms.Button();
            this.btnMailRemove = new System.Windows.Forms.Button();
            this.txtMail = new System.Windows.Forms.TextBox();
            this.lstKayitlilar = new System.Windows.Forms.ListBox();
            this.btnTemizle = new System.Windows.Forms.Button();
            this.txtPhone = new System.Windows.Forms.MaskedTextBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(22, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Adı";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Soyadı";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Doğum Tarihi";
            // 
            // lstPhones
            // 
            this.lstPhones.FormattingEnabled = true;
            this.lstPhones.Location = new System.Drawing.Point(8, 164);
            this.lstPhones.Name = "lstPhones";
            this.lstPhones.Size = new System.Drawing.Size(120, 95);
            this.lstPhones.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 148);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Telefonlar";
            // 
            // lstEmails
            // 
            this.lstEmails.FormattingEnabled = true;
            this.lstEmails.Location = new System.Drawing.Point(200, 165);
            this.lstEmails.Name = "lstEmails";
            this.lstEmails.Size = new System.Drawing.Size(120, 95);
            this.lstEmails.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(208, 149);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Emailleri";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(138, 6);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(100, 20);
            this.txtName.TabIndex = 7;
            // 
            // txtLastName
            // 
            this.txtLastName.Location = new System.Drawing.Point(138, 29);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(100, 20);
            this.txtLastName.TabIndex = 8;
            // 
            // dtpBirthDate
            // 
            this.dtpBirthDate.Location = new System.Drawing.Point(138, 51);
            this.dtpBirthDate.Name = "dtpBirthDate";
            this.dtpBirthDate.Size = new System.Drawing.Size(200, 20);
            this.dtpBirthDate.TabIndex = 9;
            // 
            // btnKaydet
            // 
            this.btnKaydet.Location = new System.Drawing.Point(8, 265);
            this.btnKaydet.Name = "btnKaydet";
            this.btnKaydet.Size = new System.Drawing.Size(161, 23);
            this.btnKaydet.TabIndex = 10;
            this.btnKaydet.Text = "Kaydet";
            this.btnKaydet.UseVisualStyleBackColor = true;
            this.btnKaydet.Click += new System.EventHandler(this.btnKaydet_Click);
            // 
            // btnPhoneAdd
            // 
            this.btnPhoneAdd.Location = new System.Drawing.Point(114, 123);
            this.btnPhoneAdd.Name = "btnPhoneAdd";
            this.btnPhoneAdd.Size = new System.Drawing.Size(38, 23);
            this.btnPhoneAdd.TabIndex = 11;
            this.btnPhoneAdd.Text = "+";
            this.btnPhoneAdd.UseVisualStyleBackColor = true;
            this.btnPhoneAdd.Click += new System.EventHandler(this.btnPhoneAdd_Click);
            // 
            // btnMailAdd
            // 
            this.btnMailAdd.Location = new System.Drawing.Point(326, 123);
            this.btnMailAdd.Name = "btnMailAdd";
            this.btnMailAdd.Size = new System.Drawing.Size(38, 23);
            this.btnMailAdd.TabIndex = 12;
            this.btnMailAdd.Text = "+";
            this.btnMailAdd.UseVisualStyleBackColor = true;
            this.btnMailAdd.Click += new System.EventHandler(this.btnMailAdd_Click);
            // 
            // btnPhoneRemove
            // 
            this.btnPhoneRemove.Location = new System.Drawing.Point(135, 194);
            this.btnPhoneRemove.Name = "btnPhoneRemove";
            this.btnPhoneRemove.Size = new System.Drawing.Size(38, 23);
            this.btnPhoneRemove.TabIndex = 13;
            this.btnPhoneRemove.Text = "-";
            this.btnPhoneRemove.UseVisualStyleBackColor = true;
            this.btnPhoneRemove.Click += new System.EventHandler(this.btnPhoneRemove_Click);
            // 
            // btnMailRemove
            // 
            this.btnMailRemove.Location = new System.Drawing.Point(326, 194);
            this.btnMailRemove.Name = "btnMailRemove";
            this.btnMailRemove.Size = new System.Drawing.Size(38, 23);
            this.btnMailRemove.TabIndex = 14;
            this.btnMailRemove.Text = "-";
            this.btnMailRemove.UseVisualStyleBackColor = true;
            this.btnMailRemove.Click += new System.EventHandler(this.btnMailRemove_Click);
            // 
            // txtMail
            // 
            this.txtMail.Location = new System.Drawing.Point(200, 125);
            this.txtMail.Name = "txtMail";
            this.txtMail.Size = new System.Drawing.Size(100, 20);
            this.txtMail.TabIndex = 16;
            // 
            // lstKayitlilar
            // 
            this.lstKayitlilar.FormattingEnabled = true;
            this.lstKayitlilar.Location = new System.Drawing.Point(540, 6);
            this.lstKayitlilar.Name = "lstKayitlilar";
            this.lstKayitlilar.Size = new System.Drawing.Size(208, 264);
            this.lstKayitlilar.TabIndex = 17;
            this.lstKayitlilar.SelectedIndexChanged += new System.EventHandler(this.lstKayitlilar_SelectedIndexChanged);
            // 
            // btnTemizle
            // 
            this.btnTemizle.Location = new System.Drawing.Point(200, 264);
            this.btnTemizle.Name = "btnTemizle";
            this.btnTemizle.Size = new System.Drawing.Size(147, 23);
            this.btnTemizle.TabIndex = 18;
            this.btnTemizle.Text = "Temizle";
            this.btnTemizle.UseVisualStyleBackColor = true;
            this.btnTemizle.Click += new System.EventHandler(this.btnTemizle_Click);
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(8, 125);
            this.txtPhone.Mask = "(999) 000-0000";
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(100, 20);
            this.txtPhone.TabIndex = 19;
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(755, 13);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(33, 79);
            this.btnDelete.TabIndex = 20;
            this.btnDelete.Text = "Sil";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.txtPhone);
            this.Controls.Add(this.btnTemizle);
            this.Controls.Add(this.lstKayitlilar);
            this.Controls.Add(this.txtMail);
            this.Controls.Add(this.btnMailRemove);
            this.Controls.Add(this.btnPhoneRemove);
            this.Controls.Add(this.btnMailAdd);
            this.Controls.Add(this.btnPhoneAdd);
            this.Controls.Add(this.btnKaydet);
            this.Controls.Add(this.dtpBirthDate);
            this.Controls.Add(this.txtLastName);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lstEmails);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lstPhones);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox lstPhones;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox lstEmails;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtLastName;
        private System.Windows.Forms.DateTimePicker dtpBirthDate;
        private System.Windows.Forms.Button btnKaydet;
        private System.Windows.Forms.Button btnPhoneAdd;
        private System.Windows.Forms.Button btnMailAdd;
        private System.Windows.Forms.Button btnPhoneRemove;
        private System.Windows.Forms.Button btnMailRemove;
        private System.Windows.Forms.TextBox txtMail;
        private System.Windows.Forms.ListBox lstKayitlilar;
        private System.Windows.Forms.Button btnTemizle;
        private System.Windows.Forms.MaskedTextBox txtPhone;
        private System.Windows.Forms.Button btnDelete;
    }
}

