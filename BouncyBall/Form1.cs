﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BouncyBall
{
    public partial class Form1 : Form
    {
        private int sWidth;
        private int sHeight;
        private const int _increment = 1;
        private bool xRigth = true;
        private bool yBottom = true;


        public Form1()
        {
            InitializeComponent();

            sWidth = this.Size.Width;
            sHeight = this.Size.Height;

            timer1.Interval = 100;
            timer1.Tick += Timer1_Tick;
            timer1.Start();
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            var posX = bBall.Location.X;
            var posY = bBall.Location.Y;

            if (posX + _increment == sWidth - bBall.Size.Width)
                xRigth = false;
            if (posX - _increment == 0)
                xRigth = true;
            //else
            //    xRigth = false;




            if (posY + _increment == sHeight - bBall.Size.Height)
                yBottom = false;
            //else
            //    yBottom = false;

            if (posY - _increment == 0)
                yBottom = true;


            if (xRigth)
            {
                posX += _increment;
            }
            else
            {
                posX -= _increment;
            }


            if (yBottom)
            {

                posY += _increment;
            }
            else
            {
                posY -= _increment;
            }


            bBall.Location = new Point(posX, posY);

        }

        private void bBall_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
